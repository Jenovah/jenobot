const Discord = require('discord.js')
const client = new Discord.Client()
import Ranking from './commands/Ranking'
import Help from './commands/Help'

client.on('ready', () => {
    client.user.setAvatar('img/bot.jpg')
        .catch(console.error)
})

client.on('message', async (message) => {
    let embedResponse = new Discord.MessageEmbed()
    const strSent = message.content.toLowerCase().split(' ')
    const action = strSent[0].startsWith('!') ? strSent[0] : null
    switch (action) {
        case '!ranking':
            const channel = await client.channels.fetch('1088927933697101884');
            const messages = await channel.messages.fetch({ limit: 100 });
            const ranking = new Ranking(message, embedResponse)
            ranking.action(messages)
            break
        case '!help':
            const help = new Help(message, embedResponse)
            help.action()
            break
        case null:
            break
        default:
            message.channel.send('Désolé, je ne connais pas cette commande !\r\nTu peux consulter l\'aide via la commande suivante: !help')
    }
})

client.login('NjkyNDM3NTQzNDc5NjcyODg1.Gvg1dX.E7aJgAkkSub3pSxVHvdIC-Icx134hkbC3adYtc')
