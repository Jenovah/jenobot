export default class Help {

    embedResponse = null
    message = null

    constructor(message, embedResponse) {
        this.message = message
        this.embedResponse = embedResponse
            .setColor('#BC0057')
            .setTitle('Un peu d\'aide ! ')
            .setURL('https://lecnam.net/')
            .setAuthor('JenoBot', 'https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setDescription('**__Voici les commandes disponibles:__**')
            .setThumbnail('https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setImage()
            .setTimestamp()
            .setFooter('JenoBot')
            .addFields(
                { name: '**Le classement**', value: '**!ranking** : Affiche le classement (Top 5) des images qui ont eu le plus de réactions.\r\n' },
            )
    }

    action() {
        this.message.channel.send(this.embedResponse)
    }
}