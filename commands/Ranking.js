export default class Ranking {

    embedResponse = null
    message = null

    constructor(message, embedResponse) {
        this.message = message
        this.embedResponse = embedResponse
            .setColor('#BC0057')
            .setTitle('Voici le classement des images !')
            .setAuthor('JenoBot', 'https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setDescription('Classement des images')
            .setThumbnail('https://cs11.pikabu.ru/post_img/2019/12/12/5/1576133915183631868.jpg')
            .setImage()
            .setTimestamp()
            .setFooter('JenoBot')
    }

    action(messages) {
        const results = new Map();
        messages.forEach((message) => {
            if (message.attachments.size > 0) {
                let total = 0;
                const reactions = message.reactions.cache.array();
                reactions.forEach((reaction) => {
                    total += reaction.count;
                });
                results.set(message.id, total);
            }
        });
        console.log(results)
        const sortedResults = [...results.entries()].sort((a, b) => b[1] - a[1]);
        this.sendFormattedData(sortedResults);
        sortedResults.forEach((result) => {
            console.log(`Message ${result[0]} has ${result[1]} reactions`);
        });
    }

    sendFormattedData = (sortedResults) => {
        let emptyMessage = true
        if (sortedResults) {
            sortedResults.forEach((element, key) => {
                if (key > 4) return
                const messageLink = `https://discord.com/channels/${this.message.guild.id}/${this.message.channel.id}/${element[0]}`;
                this.embedResponse.addFields(
                    { name: '**Top ' + (key + 1) + ' : **', value: `Message ([${element[0]}](${messageLink})) has ${element[1]} reactions`, inline: true },
                )
            })
            emptyMessage = false
        }

        if (!emptyMessage) {
            this.message.channel.send(this.embedResponse)
        } else {
            this.message.reply('Hmmm... Désolé je n\'ai pas trouvé d\'image.')
        }
    }
}

